REM PSEXEC from sysinternal required!
REM call ip with: iphostname.bat 126.0.0.1
REM This will loop while ip is down
REM when finish, look for hostname.txt file


@setlocal enableextensions enabledelayedexpansion
@echo off
set ipaddr=%1
:loop
set state=down
for /f "tokens=3,6" %%a in ('ping -n 1 !ipaddr!') do (
    if "%%a"=="!ipaddr!:" if "%%b"=="TTL=128" set state=up
)
echo.Link is !state!
if "!state!"=="up" (  
  psexec \\!ipaddr! hostname > hostname.txt
) ELSE (
    ping -n 6 127.0.0.1 >nul: 2>nul:
    goto :loop
)

endlocal